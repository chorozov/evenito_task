import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'
import Login from '@/views/Login.vue'
import SignUp from '@/views/SignUp.vue'
import Projects from '@/views/Projects.vue'
import Test from '@/views/Test.vue'
import ToggleBoxes from '@/views/ToggleBoxes.vue'
import About from '@/views/About.vue'
import Upload from '@/views/Upload.vue'
import Cards from '@/views/Cards.vue'
import Card from '@/views/Card.vue'
import TestingCustomDataTable from '@/views/TestingCustomDataTable.vue'


import firebase from 'firebase';

Vue.use(Router)

const router = new Router({
  mode:'history', 
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/home',
      
      name: 'Home',
      component: Home,
      meta: {
        title: 'Home Page - Example App',
        requiresAuth: true
      }
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Projects,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/test',
      name: 'Test',
      component: Test,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: '/toggle',
      name: 'ToggleBoxes',
      component: ToggleBoxes,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/cards',
      name: 'Cards',
      component: Cards,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/card/:id',
      name: 'Card',
      component: Card,
      props: true,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/testingcdt',
      name: 'TestingCustomDataTable',
      component: TestingCustomDataTable,
      meta: {
        requiresAuth: true
      }
    },

  ]
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next('home');
  else next();
});

export default router;
